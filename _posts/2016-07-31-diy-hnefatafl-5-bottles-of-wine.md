---
title: Do-It-Yourself Hnefatafl with 5 Bottles of Wine  
layout: post
comments: True
---

I've recently become intrigued with the ancient Viking game, hnefatafl.  Since my discovery of this game, I've downloaded versions of it on my
phone, read a [couple of books](http://blog.lizlam.com/2016/07/27/hnefatafl-book-reviews/) on the subject and perused many sets and sites online. Though there are some commercial sets for sale, hnefatafl isn't exactly mainstream.  You're not
going to find it at your local Walmart.  As a result, there are a lot of do-it-yourself sets out there.  Some of the ones I've seen are examples of some seriously skilled workmanship.  Others are crude but functional.  All are fun and creative.  There are only 3 types of pieces needed for hnefatafl, the king, the defenders and the attackers.  This simple setup makes it possible to construct a set out of everyday found objects.

 I love wine and often times save the corks. I have a kitchen drawer full of them.  The thought occured to me that it might be a fun evening project to make a hnefatal set out of the corks in order to play a few games.  I was already waiting for a hand-made set to come in the mail, but I figured, why wait?

So began my crafty evening.  

I grabbed a wine cork and a bread knife and started experimenting with different size cuts.  It was possible
to make 6 components per cork if you cut them horizontally at 2 cm a piece.  

![Picture of 6 cut wine cork pieces](/assets/2cm-6-pieces-cut-cork.jpg)

I decided I wanted to make a variant called [tablut](http://tafl.cyningstan.com/page/170/tablut) which requires 25 pieces:
8 defenders, 16 attackers and 1 king.  As it turns out, 5 bottles of wine was all I needed to accomplish this.  I happened to have
corks from Spiral Cellars (probably the best [Napa Valley Cabernet for under $10](http://thefermentedfruit.com/spiral-cellars-cabernet-review-trader-joes-wine/)).  The spiral design at the end of the corks were perfect for distinguishing defender pieces from attacker pieces.

![Cork pieces with Spiral design](/assets/spiral-defenders.jpg)

Once the attacker and defender pieces were done, I designated the king's piece by making it a little taller.  After 5 minutes of cutting, I had all the pieces needed for a tablut set.

Now all I needed was a board.

Swedish botanist Carl Linneaus is credited for having the world's [most complete description of tablut](https://archive.org/stream/ungdomsskrifter02linne#page/n155/mode/2up) and its rules.  These
rules were recorded in 1732 with a drawing of a tablut board and [playing pieces](https://archive.org/stream/ungdomsskrifter02linne#page/n157/mode/2up). I figured Carl Linneaus' diagram was just as good as
any board.  It's actually not his original drawing but an English translation of it published in 1811.  That was close enough for my purposes.  I went online, grabbed a graphic of it, printed it out and was on my way to playing a few rounds.

*NOTE: The cross section of a wine cork is about an inch, so I needed to scale accordingly when printing the board.*

![Tablut game in progress](/assets/tablut-cork-game-in-progress.jpg)

Linneaus' diagram, though historical and interesting, felt a little bland.  I thought I'd have some fun and splash a little color on it while trying to preserve the original design.  I checked the graphic into a [Perforce](https://www.perforce.com) server and experimented with colors/filters producing a slew of [revisions](https://swarm.workshop.perforce.com/files/guest/liz_lam/tablut-fun/main#commits).  Experimentations can be viewed with this Timelapse View [video](https://vimeo.com/176825722).  

I printed out the version I liked the most and voila!  Craft night is a success! I now have my own tablut/hnefatafl set to play with.  


![Up close shot of tablut board in color](/assets/tablut-corks-color-board.jpg)
