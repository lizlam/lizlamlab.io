---
title: "Game Review: Tafl and Nine Men Morris by Creative Images"
layout: post
comments: True
---

It's not easy to find a good quality hnefatafl set at a reasonable price.  *Tafl and Nine Men Morris* by Creative Images ([SevenOaksGrove on Etsy](https://www.etsy.com/shop/SevenOaksGrove)) is certainly that and exceeds expectations by essentially including 3 games in 1. Not only is there a Nine Men's Morris board on the other side but the artist also includes game instructions and pieces for Beserk Hnefatafl.  The extra glass pieces are designated with decorative crowns.  This is the first set I have come across with this variation and is definitely a unique feature to include. 

![Crowned pieces](/assets/beserk.jpg)

I'm not the biggest fan of canvas boards.  For example, the one that comes with the commercially produced *[Viking Game](http://www.history-craft.co.uk/resin-casting-products/game-pieces-boards/hnefatafl-viking-game-pieces.aspx)* is a bit too flimsy for my taste (though the Lewis Chessmen inspired pieces that come with it are beautiful). But the canvas used for this set is thick, durable, beautifully designed and printed.  The graphics are bold and the customer service is awesome.
In addition to the Beserk rules, this set also includes printed instructions on how to play with Fetlar and
Copenhagen rules.  These are the two common rule sets used in tournaments.

As with many products sold on Etsy, this is a hand-made item in which no two are going to be exactly alike.  Despite that, the set is very professionally made.  The company is ran by
a guy in Walla Walla, Washington who obviously takes a lot of pride in his work. 

Overall this was a great find.  I picked it up for my 7 year old nephew who instantly loved it when I gave it to 
him.  It's portable, beautiful and a reasonably priced($25) hnefatafl set that anyone can enjoy.

![Tafl set](/assets/tafl-creative-images.jpg)


