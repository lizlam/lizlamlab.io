---
title: Three Cork Brandub
layout: post
comments: True
---

In a previous post, I've described how I made a [tablut game with 5 bottles of wine](http://blog.lizlam.com/2016/07/31/diy-hnefatafl-5-bottles-of-wine/) (well not really the
 bottles of wine but the corks). 
I thought it would be fun to try making another hnefatafl set but wanted to try something different.  
The corks I used before just happened to have designs on the ends making them perfect for designating 
two types of pieces without extra effort.   But not all corks are decorated as such, so I started thinking 
about ways I could color them.  

Rummaging around the house, I found an ink set for stamping and calligraphy.  I took a look in my kitchen drawer and
found 3 wine corks lacking decorative ends from [Louis M. Martini](https://www.louismartini.com/) (Which by the way is a great winery to visit if you're ever in the Napa Valley area). 3 corks is perfect for making game components for an Irish variant of hnefatafl called [brandub](http://tafl.cyningstan.com/page/171/brandub).  It is the smallest of all the hnefatafl games, requiring just 13 pieces: 4 defenders, 8 attackers and 1 king.  After cutting the corks, I inked the cross section and found the  corks absorbed the color quite well.  I put them aside and let them dry for 20 minutes but noticed when I picked them up the ink still rubbed off onto my finger.  So I grabbed a bottle of Elmer's glue, put a thin white layer on the inked side and let it dry again.  Elmer's  glue, for the most part dries pretty clear and it seemed to do the job of sealing the color relatively well.   

Brandub is played on a 7x7 squared board.  In 1932, one was unearthed in Ballinderry, Ireland dating back to the 
10th century.  There is some debate on whether or not this board was used to play brandub or some other games.  But
there seems to be enough evidence in it's design to support brandub play.  Having an affinity for history and its artifacts,  I
grabbed an image online of the Ballinderry board, printed it out and used it for my makeshift brandub set. Obviously playing with cork pieces on printed paper isn't as nice as playing on a high quality set.  But with all the different
hnefatafl variations out there, it's nice to experiment and see which version you like before buying one.

![Ballinderry Board](/assets/corks-on-ballinderry-board.jpg)
