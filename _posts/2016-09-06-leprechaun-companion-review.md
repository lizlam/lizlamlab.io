---
title: A Review of The Leprechaun Companion
layout: post
comments: True
---

The Leprechaun Companion is a wonderfully illustrated and imaginative book.
It was written by Nigel Suckling, though you would be hard-pressed to actually find a copy with that name on it. He
wrote this particular book under the pen name, Niall Macnamara, the name given to him by the leprechauns.

I originally purchased this book in order to read a bit more about [Magpie](http://tafl.cyningstan.com/download/968/magpie-leaflet), a modern variation of [brandub](http://tafl.cyningstan.com/page/171/brandub).
Brandub is an Irish variant of hneftafl played on a 7x7 board.  The author takes brandub and reimagines it with a leprechaun theme.  The king is replaced with a leprechaun who is only allowed to move one space at a time due to the weight of his pot of gold.  The defenders are the leprechaun's friends assisting him to escape to a corner and the attackers are thieves trying to steal the gold.  Gameplay is essentially the same as brandub with this exception (one space moves for the king piece) and the leprechaun needs to be surrounded on 4 sides in order to be captured (or 3 sides if standing next to the central square, corner squares or edge).     

This book turned out to be a pleasant surprise.  Not only did it contain this cute backstory about Magpie (the leprechaun's favorite game), it also included a comprehensive set of rules with diagrams and two beautiful usable boards in the inside covers (you will need to provide your own pieces).  

![Magpie](/assets/lp-magpie.jpg)

The chapters are filled with delightful illustrations and
stories about these magical little creatures.  The author brings you into a world where fairies are real and you better pay attention to the leprechaun living in your house.  Though not exactly a children's book, the mix of fantasy, history and Irish folklore will certainly entertain the child in you. 

![Book cover](/assets/lp-cover.jpg)


