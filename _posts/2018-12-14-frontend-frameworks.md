---
title: "Working with Front-end Frameworks"
layout: post
comments: True
---

In the last year, I've had the privilege of being able to work on two very different and distinct
projects back to back.  I was initially hired at SKUR to take over the ownership and development
of a very large LAMP stack codebase.  The developer before me was a PHP wizard and authored the
entire web application from scratch.  No MVC frameworks, no front-end frameworks, a few 3rd party libraries for things like websockets and a custom Javascript library.

At the same time, a complete rewrite of the application was happening using React on the front-end with a GraphQL/MongoDB backend.  After a few months of maintaining the LAMP application, I was quickly moved to work on the new stuff.     

React has a bit of learning curve and part of the education process for me was also having learn modern/ES6 Javascript.  After going through many tutorials and immersing myself in the code, I was able to make meaningful contributions.

The development experience between the two codebases was as different as night and day.  I discovered the power and ability of being able to break down the UI into components and the flexibility of
being able to style them directly in the Javascript files.  I also enjoyed not having to worry about re-rendering pages or bits of pages when there were state changes.    

Although it is really impressive to be able to write an application from scratch without any frameworks, there are tons of benefits of utilizing certain conventions.  I look forward to continue working with front-end frameworks so that I can focus on building applications and delegating the things I don't need to think about.
