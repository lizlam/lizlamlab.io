---
title: A Review On The Hnefatafl (Viking Chess) Books
layout: post
related: [
    "Do-it-Yourself Hnefatafl with 5 Bottles of Wine",
    "Three Cork Brandub",
    "A Review of The Lewis Chessmen Unmasked"
]
comments: True
---

I grew up playing board games.  My uncle taught me at a very early age how to play classic
Western games such as chess and checkers.  He also taught me how to play games relating to our Chinese
culture and heritage such as [dou shou qui](https://en.wikipedia.org/wiki/Jungle_(board_game))
and [xiang qi](https://en.wikipedia.org/wiki/Xiangqi).  So it was only natural when I stumbled upon
the ancient Viking game of hfneftafl that I would instantly be fascinated by it.

Hnefatafl is a predecessor of chess.  It is simpler to play but just as addicting.  The first thing that is very apparent is that there are not a lot of sources about the history and roots of hnefatafl
in general.  Where there are tons of books about games such as chess and backgammon, book resources
on this Viking game is almost [NaN](https://en.wikipedia.org/wiki/NaN).  Which is why I was pleased to discover these two books
by Damian Walker, [An Introduction to Hnefatafl](http://tafl.cyningstan.com/weblinks/1346/an-introduction-to-hnefatafl) and
[Reconstructing Hnefatafl](http://tafl.cyningstan.com/weblinks/1068/reconstructing-hnefatafl).

*An Introduction to Hnefatafl* is exactly what it sounds like.  This little book is a great introduction
to the history of the ancient board game.  It also describes the rules of play.  It's a quick read packed with a lot of interesting facts. 
It surveys quite a bit, covering topics from its mysterious origins to its modern revival.  In some ways, the book really paints a broad picture about the migration of
the Vikings and the game they left behind.  In just 43 pages, the book fueled my interest and made me want
to learn more.

*Reconstructing Hnefatafl* takes on a more academic approach (though just as readable).  It goes through different
historical texts and reconstructs the rules of different variations of the game.  For example, the author takes a
diary written in 1732 with a fairly complete description of the game of tablut (a variation of hnefatafl) and reconstructs the rules accordingly.  Some historical texts are less comprehensive, in which Walker walks (no pun intended) us through the steps of how he fills in the gaps.  He also takes archaelogical
evidence, such as excavated game boards and meticulously deduces game play from them.  This is another short read and in 64 pages I was able to learn more about this fascinating game.

Overall, both these books are a great way to learn about hnefatafl.  The game itself is pretty fun
and these books will definitely enhance your appreciation (and your geek factor) for this little known gem.
